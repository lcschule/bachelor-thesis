# Deploy Confidential VM (CVM) With ARM
For more in-depth information on what ARM is, see [here](https://learn.microsoft.com/en-us/azure/azure-resource-manager/templates/overview).

This how-to is based on [Microsoft documentation](https://learn.microsoft.com/en-us/azure/confidential-computing/quick-create-confidential-vm-arm-amd), but is further modified to suit specific CVM needs.


[[_TOC_]]


## Prerequisities
[Azure subscription - billed account.](https://azure.microsoft.com/en-us/)

If **not** using Azure Cloud Shell:
1. [Install and configure Azure CLI.](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli)
2. Sign in to your Azure account in the Azure CLI by running: `az login`
3. Set your Azure subscription by running: `az account set --subscription <subscription-id>`


## Setup
1. Copy the example parameter file `parameters.json` located in this GitLab directory into your environment.
2. Edit the code in the file as needed. Importantly, set your complete public key as the value of the `adminPasswordOrKey` parameter. Use quotation marks around the key.


## Deployment
1. Set the variables for your confidential VM. Replace the sample values with your own information.

```
$deployName="<deployment-name>"
$resourceGroup="<resource-group-name>"
$vmName= "<confidential-vm-name>"
$region="North Europe"
```

2. If the resource group you specified does not exist, create a resource group with that name.

```
az group create -n $resourceGroup -l $region
```

3. Deploy your VM to Azure using an ARM template with the parameter file created earlier.

```
az deployment group create `
 -g $resourceGroup `
 -n $deployName `
 -u "https://aka.ms/CVMTemplate" `
 -p "<json-parameter-file-path>" `
 -p vmLocation=$region `
    vmName=$vmName
```


## Review
You can *list* your deployed resources with the command below.

```
az resource list --resource-group <resourcegroup-name>
```


## Cleanup
ARM offers no integrated command to delete all resources that were created in a specific deployment. Therefore, one will need to delete these either through the portal's GUI, or by manually deleting each resource through the CLI.

To do the latter, first delete each individual resource. For example, the code below can be used for deletion of a VM.

```
az resource delete \
  --resource-group ExampleResourceGroup \
  --name ExampleVM \
  --resource-type "Microsoft.Compute/virtualMachines"
```

Then, delete the resource group.

```
az group delete --name ExampleResourceGroup
```
