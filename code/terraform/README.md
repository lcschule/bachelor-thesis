# Deploy Confidential VM (CVM) With Terraform
For more in-depth information on what Terraform is, see [here](https://developer.hashicorp.com/terraform/intro).

This how-to is based on [Microsoft documentation](https://learn.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-terraform) and [HashiCorp documentation](https://developer.hashicorp.com/terraform/tutorials/azure-get-started/azure-build).


[[_TOC_]]


## Prerequisities
[Azure subscription - free account or billed account.](https://azure.microsoft.com/en-us/)

If **not** using Azure Cloud Shell: \
[Install and configure Terraform.](https://learn.microsoft.com/en-us/azure/developer/terraform/quickstart-configure) 


## Setup
1. Decide what type of technology you want to use when creating the VM. Currently, AMD SEV and Intel SGX are supported.
2. Clone the correspondingly named folder to Azure Cloud Shell or to your computer, using e.g. Bash or PowerShell.
3. Make sure the files `providers.tf`, `main.tf`, `variables.tf` and `outputs.tf` are inside the folder.


## Initialization
Initialize the Terraform configuration. This command downloads the Azure provider required to manage your Azure resources.

```
terraform init
```


## Validation
Check if your configuration files have consistent formatting. This will automatically update configurations in the current directory for readability and consistency.

```
terraform fmt
```

Furthermore, you can check if your configuration is syntactically valid.

```
terraform validate
```


## Deployment
Create an execution plan.

```
terraform plan -out main.tfplan
```

Apply the execution plan to your cloud infrastructure. 

```
terraform apply main.tfplan
```


## Inspection
On applying your configuration, Terraform writes data into a file called *terraform.tfstate*. Inspect the current state.

```
terraform show
```

To review the information in your state file, use the *state* command. A list of available commands will be presented by doing so.  

```
terraform state
```

To just get a list of resources in the state, use *state list*.

```
terraform state list
```


## Verification
To use SSH to connect to the virtual machine, do the following steps:

1. Get the SSH private key and save it to a file.

```
terraform output -raw tls_private_key > id_rsa
```


2. Set necessary read and write permissions for newly created key file.

```
chmod 600 id_rsa
```


3. Get the virtual machine public IP address.

```
terraform output public_ip_address
```


4. Use SSH to connect to the virtual machine.

```
ssh -i id_rsa azureuser@<public_ip_address>
```


## Cleanup
When you no longer need the resources created via Terraform, do the following steps:

1. Create a plan for destroying the resources.

```
terraform plan -destroy -out main.destroy.tfplan
```


2. Apply the execution plan.

```
terraform apply main.destroy.tfplan
```
