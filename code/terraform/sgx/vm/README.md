# Intel SGX
The files in this directory serve as the basis for setting up a CC environment, utilizing the Intel Software Guard Extensions (SGX) technology.

The code is based on [Microsoft documentation](https://learn.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-terraform). \
Modifications of the Terraform template files from this documentation are listed in the section below.

## Changelog
File *main.tf*:
- **Line 52-62**: Added another security rule that allows HTTP traffic on port 80
- **Line 112**: Name of the virtual machine has been changed from "myVM" to "ccIntelVM"
- **Line 116**: The VM size has been changed from "Standard_DS1_v2" to "Standard_DC1s_v2", a requirement for confidential compute
- **Line 118**: Added 'custom_data = filebase("oe.sh")' to have a script running after VM deployment (installs OE)
- **Line 123**: OS disk storage account type has been changed from "Premium_LRS" to "StandardSSD_LRS"
- **Line 128**: The "offer" property has been changed from "0001-com-ubuntu-server-jammy" to "0001-com-ubuntu-server-focal"
- **Line 129**: The "sku" property has been changed from "22_04-lts-gen2" to "20_04-lts-gen2"
 
File *variables.tf*:
- **Line 3**: Default resource group location has been changed from "eastus" to "northeurope"
