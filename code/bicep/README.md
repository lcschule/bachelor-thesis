# Deploy Confidential VM (CVM) With Bicep
For more in-depth information on what Bicep is, see [here](https://learn.microsoft.com/en-us/azure/azure-resource-manager/bicep/overview).

This how-to is based on [Microsoft documentation](https://learn.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-bicep?tabs=CLI), but is further modified to suit specific CVM needs.


[[_TOC_]]


## Prerequisites
[Azure subscription - billed account.](https://azure.microsoft.com/en-us/)

If **not** using Azure Cloud Shell:
1. [Install and configure Azure CLI.](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli)
2. Sign in to your Azure account in the Azure CLI by running: `az login`
3. Set your Azure subscription by running: `az account set --subscription <subscription-id>`
4. [Install Bicep tools.](https://learn.microsoft.com/en-us/azure/azure-resource-manager/bicep/install)


## Setup
Create a directory named `bicep`. Enter the directory and create a file named `main.bicep`. Copy the contents of the `main.bicep` file found in this GitLab repository to the newly created file.


## Deployment
The following commands are specific for Azure CLI. For the identical Azure PowerShell commands, see [Microsoft documentation](https://learn.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-bicep?tabs=PowerShell).

1. Create the resource group.

Set a *name* for the resource group (e.g. ccAmdVM-rg).

```
az group create --name <resourcegroup-name> --location northeurope
```

2. Create and deploy the CVM.

Use the *name* of the previously created resource group. For parameters, add a *username* of your choice (e.g. azureuser), followed by your *public ssh-key*. If you do not already have a key, generate a new key with `ssh-keygen` in the command line.  

```
az deployment group create --resource-group <resourcegroup-name> --template-file main.bicep --parameters adminUsername=<admin-username> adminPasswordOrKey='<ssh-publickey>'
```


## Review
You can *list* your deployed resources with the command below.

```
az resource list --resource-group <resourcegroup-name>
```


## Cleanup
If the resources you deployed are no longer needed, you can use the next command to *delete* both the VM and the resource group. Add the *resource name* you want to be deleted. 

```
az group delete --name <resourcegroup-name>
```


## Extras
For more information about what you are able to add to your bicep file, see [Microsoft documentation](https://learn.microsoft.com/en-us/azure/templates/microsoft.compute/2022-03-01/virtualmachinescalesets?pivots=deployment-language-bicep). 
