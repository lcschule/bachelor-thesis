# Changes made to the 'main.bicep' file.

- **Line 2:**  Changed the param vmName to string 'ccAmdVM'
- **Line 12:** Changed the param authenticationType from string 'password' to string 'sshPublicKey'
- **Line 33:** Changed the param vmSize to 'Standard_DC2as_v5'
- **Line 48:** Added 'ConfidentialVM' as an allowed Security Type since it was previously not there
- **Line 50:** Changed the param securityType from string 'TrustedLaunch' to string 'ConfidentialVM'
- **Line 61:** Changed 'offer' to '0001-com-ubuntu-confidential-vm-focal' under imageReference
- **Line 62:** Changed 'sku' to '22_04-lts-cvm' under imageReference
- **Line 143-155:** Added HTTP on port 80 to allow inbound HTTP traffic
- **Line 210-212:** Added the code block securityProfile:securityEncryptionType:'VmGuestStateOnly', a necessity for creating a CVM
- **Line 230:** Changed securityProfile:securityType from 'TrustedLaunch' to 'ConfidentialVM'
- **Line 234:** Changed securityType from 'TrustedLaunch' to 'ConfidentialVM'
