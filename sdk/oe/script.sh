#!/bin/bash

# Configure the Intel and Microsoft APT Repositories
echo 'deb [arch=amd64] https://download.01.org/intel-sgx/sgx_repo/ubuntu focal main' | sudo tee /etc/apt/sources.list.d/intel-sgx.list
wget -qO - https://download.01.org/intel-sgx/sgx_repo/ubuntu/intel-sgx-deb.key | sudo apt-key add -

echo "deb http://apt.llvm.org/focal/ llvm-toolchain-focal-11 main" | sudo tee /etc/apt/sources.list.d/llvm-toolchain-focal-11.list
wget -qO - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -

echo "deb [arch=amd64] https://packages.microsoft.com/ubuntu/20.04/prod focal main" | sudo tee /etc/apt/sources.list.d/msprod.list
wget -qO - https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -

sudo apt update

# Install the Intel and Open Enclave packages and dependencies
sudo apt-get update
sudo apt -y install clang-11 libssl-dev gdb libsgx-enclave-common libsgx-quote-ex libprotobuf17 libsgx-dcap-ql libsgx-dcap-ql-dev az-dcap-client open-enclave

# Install Open Enclave CMAKE packages
sudo apt-get -y install python3-pip
sudo pip3 install cmake

# Building and Running the Samples on Linux
# Copy samples to home directory
cp -r /opt/openenclave/share/openenclave/samples ~/mysamples

# Source the file
. /opt/openenclave/share/openenclave/openenclaverc

# Create an alias for executing oeedger8r software
alias oeedger8r="/opt/openenclave/bin/oeedger8r"
