# Open Enclave tutorial
Open Enclave provides a consistent abstraction across different hardware Enclave platforms. This guide will cover the use of the Intel SGX platform only. For more information, see [Open Enclave documentation](https://github.com/openenclave/openenclave/tree/master/docs/GettingStartedDocs).


[[_TOC_]]


## Install OE SDK (Ubuntu 20.04)
For more information, see [Open Enclave documentation](https://github.com/openenclave/openenclave/blob/master/docs/GettingStartedDocs/install_oe_sdk-Ubuntu_20.04.md).


### Prerequisites
A virtual machine with a hardware enclave, e.g. Intel SGX. \
To create, follow one of the methods/documentations below:
- [GUI (through Azure portal)](https://learn.microsoft.com/en-us/azure/confidential-computing/quick-create-portal)
- [CLI (using Terraform code)](https://gitlab.stud.idi.ntnu.no/lcschule/bachelor-thesis/-/blob/main/code/README.md)

The install process can be skipped if you already [deployed an SGX VM with Terraform](https://gitlab.stud.idi.ntnu.no/lcschule/bachelor-thesis/-/tree/main/code/terraform), as this script is automatically executed after VM deployment. 
In that case, go straight to the **Build/Run samples** section.


### Platform requirements
- Ubuntu 20.04-LTS 64-bit
- SGX with support for Flexible Launch Control (FLC)


### Setup
Copy the `script.sh` bash file to your sgx VM. 
It contains the code needed to install the Open Enclave SDK.


### Install
Make sure execute rights are given to the `script.sh` file with `sudo chmod +x`. 
By executing this script, the following will be installed and configured:
- Intel and Microsoft APT repositories
- Intel and Open Enclave packages and dependencies
- [Open Enclave CMAKE packages](https://github.com/openenclave/openenclave/blob/master/cmake/sdk_cmake_targets_readme.md)
- Copy OE samples being in '/opt/openenclave/' to your home environment in order to give you write permissions
- Source the file to set up environment variables for sample building

For more information, see the ['script.sh' bash file](https://gitlab.stud.idi.ntnu.no/lcschule/bachelor-thesis/-/blob/main/sdk/oe/script.sh).

To run the bash file, type `./script.sh` in the current directory.


## Build/Run samples
For more information, see [Open Enclave documentation](https://github.com/openenclave/openenclave/blob/master/samples/BuildSamplesLinux.md).


### Building samples with CMAKE
To build a sample using CMake, change directory to your target sample directory (e.g. helloworld) and execute the following commands:

```
mkdir build && cd build
cmake ..
make
```

Run the sample with:
```
make run
```

For example, for the **helloworld** sample:

```
~/openenclave/share/openenclave/samples$ cd helloworld/
~/openenclave/share/openenclave/samples/helloworld$ mkdir build && cd build
~/openenclave/share/openenclave/samples/helloworld/build$ cmake ..
~/openenclave/share/openenclave/samples/helloworld/build$ make
~/openenclave/share/openenclave/samples/helloworld/build$ make run
```

### Building samples with GNU Make (OPTIONAL)

The Makefile in the root of each sample directory has three rules:
- **build**: Calls into the Makesfiles in the host and enclave directories to build.
- **clean**: Calls into the Makefiles in the hsot and enclave directories to clean all generated files.
- **run**: Runs the generated host executable, passing the signed enclave executable as a parameter to build a sample using GNU Make, change directory to your target sample directory and run `make build` to build the sample. Then execute `make run` to run the sample.

For example, for the **helloworld** sample:
```
~/openenclave/share/openenclave/samples$ cd helloworld/
~/openenclave/share/openenclave/samples/helloworld$ make build
~/openenclave/share/openenclave/samples/helloworld$ make run
```


### Simulation mode
Some of the samples can be run in simulation mode. The changes will not be applied in reality, but you are simulating what will happen by executing the sample. To run the sample in simulation mode, use: 
```
make simulate
```

## Troubleshooting
If facing problems during the install, please do the following for troubleshooting:

### 1. Install Intel SGX DCAP Driver
Some versions of Ubuntu come with the SGX driver already installed. You can check by running with the following:
```
`$ dmesg | grep -i sgx`
```

**Output:**
[  106.775199] sgx: intel_sgx: Intel SGX DCAP Driver {version}

If the output of the above is blank, you should proceed with installing the driver:
```
sudo apt update
sudo apt -y install dkms
wget https://download.01.org/intel-sgx/sgx-linux/2.17/distro/ubuntu20.04-server/sgx_linux_x64_driver_1.41.bin -O sgx_linux_x64_driver.bin
chmod +x sgx_linux_x64_driver.bin
sudo ./sgx_linux_x64_driver.bin
```

Check newest version of the Intel SGX DCAP driver on [Intel's SGX site](https://www.intel.com/content/www/us/en/developer/tools/software-guard-extensions/linux-overview.html#downloads).


### 2. Verify the OE SDK install
See this [link](https://github.com/openenclave/openenclave/blob/master/docs/GettingStartedDocs/Linux_using_oe_sdk.md) for verifying and using the installed SDK.
