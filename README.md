# Bachelor Thesis
This project repository demonstrates how Confidential Computing (CC) can be implemented in the cloud -- mainly utilizing Azure. It prototypes and documents numerous ways in which to setup a CC environment, each with their different traits, requirements and capabilities. The project leans heavily on the core principles of Infrastructure as Code (IaC), with the majority of code being written in Terraform. The repository contains suggested designs, as well as general documentation on how to setup and interact with any given CC environment on your own.

Below is an outline of the repository's hierarchical structure:

```
Bachelor Thesis/
│
├── code/
│   ├── arm/
│   │   ├── README.md
│   │   └── parameters.json
│   │
│   ├── bicep/
│   │   ├── README.md
│   │   ├── changes.md
│   │   └── main.bicep
│   │
│   └── terraform/
│       ├── sev/
│       │   ├── README.md
│       │   ├── main.tf
│       │   ├── outputs.tf
│       │   ├── providers.tf
│       │   └── variables.tf
│       │
│       ├── sgx/
│       │   ├── README.md
│       │   ├── main.tf
│       │   ├── outputs.tf
│       │   ├── providers.tf
│       │   └── variables.tf
│       │
│       └── README.md
│
├── docs/
│   ├── riskmatrix/
│   │   ├── riskmatrix.png
│   │   └── riskmatrix.xlsx
│   │
│   └── schedule/
│       ├── schedule.png
│       └── schedule.xlsx
│
├── sdk/
│   └── oe/
│       ├── README.md
│       └── script.sh
│
└── README.md
```

- `.../code/` contains infrastructure-as-code for deploying CC environments.
- `.../docs/` contains general-purpose documentation related to the project.
- `.../sdk/` contains software development kits for building secure applications.
